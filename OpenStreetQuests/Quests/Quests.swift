//
//  Quests.swift
//  OpenStreetQuests
//
//  Created by Elise on 05/01/2023.
//

import Foundation

struct InactiveQuestList {
    var questIDs: [String] = []
    
    private static func key(_ userInfo: UserInfo) -> String {
        return "inactiveQuests-\(OAuth2.isDevelopment ? "dev" : "prod")-\(userInfo.uid)"
    }
    
    func write(_ userInfo: UserInfo) -> Bool {
        let defaults = UserDefaults.standard

        let encoder = JSONEncoder()
        if let inactiveList = try? encoder.encode(self.questIDs) {
            defaults.set(inactiveList, forKey: Self.key(userInfo))
            return true
        }

        return false
    }
    
    static func clear(_ userInfo: UserInfo) {
        UserDefaults.standard.set(nil, forKey: Self.key(userInfo))
    }

    static func read(_ userInfo: UserInfo) -> Self {
        let defaults = UserDefaults.standard

        if let inactiveListEncoded = defaults.object(forKey: Self.key(userInfo)) as? Data {
            let decoder = JSONDecoder()
            if let inactiveList = try? decoder.decode([String].self, from: inactiveListEncoded) {
                return Self(questIDs: inactiveList)
            }
        }

        return Self()
    }
}

struct QuerySolution: Decodable {
    let query: OSMQuery
    let solution: QuestSolution
}

class Quest: Decodable {
    let name: String
    let description: String
    let importance: Int
    let colour: String
    let querySolution: [String:QuerySolution]

    static let quests: [String:Quest] = try! JSONDecoder().decode([String:Quest].self, from: Data(contentsOf: Bundle.main.url(forResource: "Quests", withExtension: "json")!))
}

struct SelectionQuestSolutionType: Decodable {
    var question: String
    var destinationKey: String
    var entries: [String:[SelectionQuestEntry]]
}

struct YesNoQuestSolutionType: Decodable {
    var question: String
    var destinationKey: String
    var yesValue: String?
    var noValue: String?
}

struct CountQuestSolutionType: Decodable {
    var question: String
    var destinationKey: String
}

enum QuestSolution: Decodable {
    case yesNo(YesNoQuestSolutionType)
    case count(CountQuestSolutionType)
    case selection(SelectionQuestSolutionType)
    
    enum TypeCodingKey: CodingKey {
        case type;
    }
    
    enum QuestType: String, Decodable {
        case yesNo = "YesNo"
        case count = "Count"
        case selection = "Selection"
        
        init(from decoder: Decoder) throws {
            let container = try decoder.singleValueContainer()
            let type = try container.decode(String.self)
            self = QuestType(rawValue: type)!
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: TypeCodingKey.self)
        guard let type = try? container.decode(QuestType.self, forKey: .type) else {
            fatalError("Quest missing valid quest type")
        }
        
        let objectContainer = try decoder.singleValueContainer()
        
        switch type {
        case .yesNo:
            let yesNo = try objectContainer.decode(YesNoQuestSolutionType.self)
            self = .yesNo(yesNo)
        case .count:
            let count = try objectContainer.decode(CountQuestSolutionType.self)
            self = .count(count)
        case .selection:
            let selection = try objectContainer.decode(SelectionQuestSolutionType.self)
            self = .selection(selection)
        }
    }
}
