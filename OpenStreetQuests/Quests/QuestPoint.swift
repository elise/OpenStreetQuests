//
//  QuestPoint.swift
//  OpenStreetQuests
//
//  Created by Elise on 05/01/2023.
//

import Foundation
import TangramMap

struct QuestPoint {
    var quest: Quest
    var location: CLLocationCoordinate2D
    var properties: [String:String]
}

class QuestPointsMapLayer {
    static let layerName: String = "osq_points"
    var layer: TGMapData
    
    var questPoints: [QuestPoint] = []
    
    func removeQuest(elementID: ElementID, questID: String) {
        switch elementID {
        case .node(let nodeID):
            let nodeIDStr = String(nodeID)
            self.questPoints.removeAll { quest in
                return quest.properties["nodeID"] == nodeIDStr && quest.properties["questID"] == questID
            }
        case .way(let wayID):
            let wayIDStr = String(wayID)
            self.questPoints.removeAll { quest in
                return quest.properties["wayID"] == wayIDStr && quest.properties["questID"] == questID
            }
        }
        self.set(self.questPoints)
    }
    
    init(_ mapView: TGMapView) {
        self.layer = mapView.addDataLayer(Self.layerName, generateCentroid: false)!
    }
    
    func set(_ points: [QuestPoint]) {
        self.questPoints = points
        let featureCollection = points.map({ (element) in
            var properties = [
                "colour": element.quest.colour,
                "importance": String(element.quest.importance),
            ]
            
            for (key, value) in element.properties {
                properties[key] = value
            }
            
            return TGMapFeature(point: element.location, properties: properties)
        })
        
        self.layer.setFeatures(featureCollection)
    }
    
    func clear() {
        self.questPoints.removeAll()
        self.layer.clear()
    }
}
