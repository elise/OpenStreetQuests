//
//  OAuth2.swift
//  OpenStreetQuests
//
//  Created by Elise on 08/01/2023.
//

import Foundation

struct UserInfo: Codable {
    let username: String
    let uid: Int
    let token: String
    
    private static var key: String {
        get {
            OAuth2.isDevelopment ? "userInfo-dev" : "userInfo"
        }
    }

    func write() -> Bool {
        let defaults = UserDefaults.standard

        let encoder = JSONEncoder()
        if let encodedConfig = try? encoder.encode(self) {
            defaults.set(encodedConfig, forKey: Self.key)
            return true
        }

        return false
    }
    
    static func clear() {
        UserDefaults.standard.set(nil, forKey: Self.key)
    }

    static func read() -> Self? {
        let defaults = UserDefaults.standard

        if let encodedConfig = defaults.object(forKey: Self.key) as? Data {
            let decoder = JSONDecoder()
            if let config = try? decoder.decode(Self.self, from: encodedConfig) {
                return config
            }
        }

        return nil
    }
}

enum TradeCodeError: String, Error {
    case InvalidTokenResponse = "errors.codeTrade.InvalidResponse"
    case BadStatusCode = "errors.codeTrade.InvalidStatusCode"
}

class OAuth2 {
    static let stateCharmap: [Character] = Array("abcdefghijklmnopqrstuvwxyz123456789")
    
    static let redirectUri: String = "openstreetquests://oauth2"
    static let isDevelopment: Bool = false
    
    static let prodClientID: String = "EG1JtZJKUNaPvOgFZmC-Hus2tKxBUKir_HY_qLKr6c8"
    static let prodClientSecret: String = Bundle.main.infoDictionary?["ProdOSMOAuth2Secret"] as! String
    static let prodAuthUrl: URL = URL(string: "https://www.openstreetmap.org/oauth2/authorize")!
    static let prodTokenUrl: URL = URL(string: "https://www.openstreetmap.org/oauth2/token")!
    
    static let devClientID: String = "ZY8NuVTseqIHr7qfJeKR8d6qoD3aVIOEV3CfhUZOUxY"
    static let devClientSecret: String = Bundle.main.infoDictionary?["DevOSMOAuth2Secret"] as! String
    static let devAuthUrl: URL = URL(string: "https://master.apis.dev.openstreetmap.org/oauth2/authorize")!
    static let devTokenUrl: URL = URL(string: "https://master.apis.dev.openstreetmap.org/oauth2/token")!
    
    static var clientID: String {
        get {
            return Self.isDevelopment ? Self.devClientID : Self.prodClientID
        }
    }
    
    static var clientSecret: String {
        get {
            return Self.isDevelopment ? Self.devClientSecret : Self.prodClientSecret
            
        }
    }
    
    static var authURL: URL {
        get {
            return Self.isDevelopment ? Self.devAuthUrl : Self.prodAuthUrl
        }
    }
    
    static var tokenURL: URL {
        get {
            return Self.isDevelopment ? Self.devTokenUrl : Self.prodTokenUrl
        }
    }
    
    static var state: String? {
        get {
            return UserDefaults.standard.string(forKey: "oauthState")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "oauthState")
        }
    }
    
    static func setupState() -> String {
        var state = ""
        
        for _ in 0..<16 {
            let rand = Int.random(in: 0..<Self.stateCharmap.count)
            state.append(Self.stateCharmap[rand])
        }
        
        Self.state = state
        return state
    }
    
    static func authenticateUrl() -> URL {
        let state = Self.setupState()
        
        let url = Self.authURL.appending(queryItems: [
            URLQueryItem(name: "response_type", value: "code"),
            URLQueryItem(name: "client_id", value: Self.clientID),
            URLQueryItem(name: "redirect_uri", value: Self.redirectUri),
            URLQueryItem(name: "scope", value: "read_prefs write_prefs write_api write_notes"),
            URLQueryItem(name: "state", value: state)
        ])
        
        return url
    }
    
    static func tradeCode(_ code: String) async throws -> UserInfo {
        let url = Self.tokenURL
        
        let codeEncoded = code.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let redirectUriEncoded = self.redirectUri.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let basicAuth = "\(self.clientID):\(self.clientSecret)"
            .data(using: String.Encoding.utf8)!
            .base64EncodedString()
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Basic \(basicAuth)", forHTTPHeaderField: "Authorization")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = "grant_type=authorization_code&code=\(codeEncoded)&redirect_uri=\(redirectUriEncoded)".data(using: .utf8)
        
        let (data, response) = try await URLSession.shared.data(for: request)
        let httpResponse = (response as? HTTPURLResponse)!
        
        if httpResponse.statusCode != 200 {
            print("Trading code returned status code \(httpResponse.statusCode)")
            throw TradeCodeError.BadStatusCode
        }
        
        guard let tokenResponse = try? JSONDecoder().decode(AccessTokenJsonResponse.self, from: data) else {
            print("Failed to decode access token response")
            throw TradeCodeError.InvalidTokenResponse
        }
        
        return try await OSMUtils.userDetails(tokenResponse.access_token)
    }
    
    private struct AccessTokenJsonResponse: Decodable {
        var access_token: String
    }
}
