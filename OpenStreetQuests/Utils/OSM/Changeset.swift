//
//  Changeset.swift
//  OpenStreetQuests
//
//  Created by Elise on 08/01/2023.
//

import Foundation
import SwiftyXML
import CoreLocation

struct CodableChangeset: Codable {
    var modify: [QuestWrappedElement] = []
    
    init(_ userInfo: UserInfo) {
        if let read = Self.read(userInfo) {
            self.modify = read.modify
        }
    }
    
    static func url(_ userInfo: UserInfo) -> URL {
        return FilesystemUtils.documentURL(subpath: "changeset-\(userInfo.uid).json")
    }
    
    static func read(_ userInfo: UserInfo) -> Self? {
        let url = Self.url(userInfo)
        
        return FilesystemUtils.readJSON(path: url, removeWhenInvalid: true)
    }
    
    func write(_ userInfo: UserInfo) {
        let url = Self.url(userInfo)
        
        FilesystemUtils.writeJSON(path: url, value: self)
    }
}

class CurrentChangeset: ObservableObject {
    static let version = 0.6
    static let generator = "OpenStreetQuests v\((Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String)!)"
    
    @Published var modify: [QuestWrappedElement] = []
    
    var codableState: CodableChangeset
    let userInfo: UserInfo
    
    init(userInfo: UserInfo) {
        self.userInfo = userInfo
        self.codableState = CodableChangeset(userInfo)
        self.modify = self.codableState.modify
    }

    func fetchElement(_ elementID: ElementID) async throws -> (OSMCodableElement?, Bool) {
        let finalElementOpt: QuestWrappedElement? = self.codableState.modify.last { (element) in
            switch (elementID, element.element) {
            case (.way(let wayID), .way(let way)):
                return way.id == wayID
            case (.node(let nodeID), .node(let node)):
                return node.id == nodeID
            default:
                return false
            }
        }
                                                            
        if let element = finalElementOpt {
            return (element.element, true)
        }
        
        switch elementID {
        case .node(let nodeID):
            return ((try await OSMNode.fetch(nodeID))?.intoElement(), false)
        case .way(let wayID):
            return ((try await OSMWay.fetch(wayID))?.intoElement(), false)
        }
    }
    
    func isSolved(_ elementID: ElementID, _ questID: String) -> Bool {
        return self.codableState.modify.first { element in
            switch (elementID, element.element) {
            case (.way(let wayID), .way(let way)):
                return way.id == wayID && element.questID == questID
            case (.node(let nodeID), .node(let node)):
                return node.id == nodeID && element.questID == questID
            default:
                return false
            }
        } != nil
    }
    
    func createChangeset(_ count: Int) async throws -> Int {
        let url = OSMUtils.latestVersionedApiUrl("/changeset/create")
        
        let changesetXML = XML(name: "osm")
            .addChild(
                XML.init(name: "changeset")
                    .addChildren([
                        XML.init(
                            name: "tag",
                            attributes: [
                                "k": "created_by",
                                "v": CurrentChangeset.generator
                            ]),
                        XML.init(
                            name: "tag",
                            attributes: [
                                "k": "comment",
                                "v": "Solve \(String(count)) \(count == 1 ? "quest" : "quests") using OpenStreetQuests"
                            ]),
                        XML.init(
                            name: "tag",
                            attributes: [
                                "k": "source",
                                "v": "survey"
                            ])
                    ])
            ).toXMLString()
        var request = URLRequest(url: url)
        
        request.httpMethod = "PUT"
        request.addValue("Bearer \(self.userInfo.token)", forHTTPHeaderField: "Authorization")
        request.setValue("text/xml", forHTTPHeaderField: "Content-Type")
        request.httpBody = changesetXML.data(using: .utf8)
        
        let (data, response) = try await URLSession.shared.data(for: request)
        let httpResponse = (response as? HTTPURLResponse)!
        
        if httpResponse.statusCode != 200 {
            print("Initiating changeset returned status code \(httpResponse.statusCode)")
            throw URLError(.badServerResponse, userInfo: ["message": "Recieved status code \(httpResponse.statusCode)"])
        }
        
        guard let changesetIDString = String(data: data, encoding: .utf8) else {
            print("Server returned invalid data when initiating changeset...")
            throw URLError(.badServerResponse, userInfo: ["message": "Recieved bad data"])
        }
        
        guard let changesetID = Int(changesetIDString) else {
            print("Server returned a non-int when initiating changeset...")
            throw URLError(.badServerResponse, userInfo: ["message": "Recieved non-int"])
        }
        
        return changesetID
    }
    
    func removeUpdate(_ elementID: ElementID, _ questID: String) async throws {
        var previousIdx: Int?
        var nextIdxs: [Int] = []
        var index: Int?
        
        switch elementID {
        case .way(let wayID):
            for elementIdx in self.codableState.modify.indices {
                let element = self.codableState.modify[elementIdx]
                
                if element.questID == questID && element.element.elementID() == elementID {
                    index = elementIdx
                } else if element.element.elementID() == elementID {
                    if index == nil {
                        previousIdx = elementIdx
                    } else {
                        nextIdxs.append(elementIdx)
                        break
                    }
                }
            }
            
            guard let index = index else {
                return
            }
            
            if nextIdxs.isEmpty {
                self.codableState.modify.remove(at: index)
                self.codableState.write(self.userInfo)
                DispatchQueue.main.async {
                    self.modify = self.codableState.modify
                }
                return
            }
            
            let previous: OSMWay
            if let previousIdx = previousIdx {
                switch self.codableState.modify[previousIdx].element {
                case .way(let way):
                    previous = way
                default:
                    fatalError("previousIdx type mismatch")
                }
            } else if let fetchedWay = try await OSMWay.fetch(wayID) {
                previous = fetchedWay
            } else {
                return
            }
            
            let toRemove: OSMWay
            
            switch self.codableState.modify[index].element {
            case .way(let way):
                toRemove = way
            default:
                fatalError("element type mismatch")
            }
            
            var changedFields: [String:(String, String)] = [:]
            var newFields: [String:String] = [:]
            
            for (key, value) in toRemove.tags ?? [:] {
                if let oldVal = previous.tags?[key] {
                    changedFields[key] = (oldVal, value)
                } else {
                    newFields[key] = value
                }
            }
            
            var state = self.codableState.modify

            for nextIdx in nextIdxs {
                var tags: [String:String]
                var way: OSMWay
                
                switch state[nextIdx].element {
                case .way(let sWay):
                    tags = sWay.tags ?? [:]
                    way = sWay
                default:
                    fatalError("element type mismatch in nextIdxs")
                }
                
                for (key, value) in newFields {
                    if tags[key] == value {
                        tags.removeValue(forKey: key)
                    }
                }
                
                for (key, (originalValue, newValue)) in changedFields {
                    if tags[key] == newValue {
                        tags[key] = originalValue
                    } else {
                        changedFields.removeValue(forKey: key)
                    }
                }
                
                for (key, value) in newFields {
                    if tags[key] == value {
                        tags.removeValue(forKey: key)
                    } else {
                        newFields.removeValue(forKey: key)
                    }
                }
                
                way.version -= 1
                way.tags = tags
                
                var element = state[nextIdx]
                element.element = .way(way)
                state[nextIdx] = element
            }
            
            state.remove(at: index)
            let nonMutState = state
            self.codableState.modify = nonMutState
            self.codableState.write(self.userInfo)
            DispatchQueue.main.async {
                self.modify = nonMutState
            }
        case .node(let nodeID):
            for elementIdx in self.codableState.modify.indices {
                let element = self.codableState.modify[elementIdx]
                
                if element.questID == questID && element.element.elementID() == elementID {
                    index = elementIdx
                } else if element.element.elementID() == elementID {
                    if index == nil {
                        previousIdx = elementIdx
                    } else {
                        nextIdxs.append(elementIdx)
                        break
                    }
                }
            }
            
            guard let index = index else {
                return
            }
            
            if nextIdxs.isEmpty {
                self.codableState.modify.remove(at: index)
                self.codableState.write(self.userInfo)
                DispatchQueue.main.async {
                    self.modify = self.codableState.modify
                }
                return
            }
            
            let previous: OSMNode
            if let previousIdx = previousIdx {
                switch self.codableState.modify[previousIdx].element {
                case .node(let node):
                    previous = node
                default:
                    fatalError("previousIdx type mismatch")
                }
            } else if let fetchedNode = try await OSMNode.fetch(nodeID) {
                previous = fetchedNode
            } else {
                return
            }
            
            let toRemove: OSMNode
            
            switch self.codableState.modify[index].element {
            case .node(let node):
                toRemove = node
            default:
                fatalError("element type mismatch")
            }
            
            var changedFields: [String:(String, String)] = [:]
            var newFields: [String:String] = [:]
            
            for (key, value) in toRemove.tags ?? [:] {
                if let oldVal = previous.tags?[key] {
                    changedFields[key] = (oldVal, value)
                } else {
                    newFields[key] = value
                }
            }
            
            var state = self.codableState.modify
            
            for nextIdx in nextIdxs {
                var tags: [String:String]
                var node: OSMNode
                
                switch state[nextIdx].element {
                case .node(let sNode):
                    tags = sNode.tags ?? [:]
                    node = sNode
                default:
                    fatalError("element type mismatch in nextIdxs")
                }
                
                for (key, value) in newFields {
                    if tags[key] == value {
                        tags.removeValue(forKey: key)
                    }
                }
                
                for (key, (originalValue, newValue)) in changedFields {
                    if tags[key] == newValue {
                        tags[key] = originalValue
                    } else {
                        changedFields.removeValue(forKey: key)
                    }
                }
                
                for (key, value) in newFields {
                    if tags[key] == value {
                        tags.removeValue(forKey: key)
                    } else {
                        newFields.removeValue(forKey: key)
                    }
                }
                
                node.version -= 1
                node.tags = tags
                
                var element = state[nextIdx]
                element.element = .node(node)
                state[nextIdx] = element
            }
            
            state.remove(at: index)
            let nonMutState = state
            self.codableState.modify = nonMutState
            self.codableState.write(self.userInfo)
            DispatchQueue.main.async {
                self.modify = nonMutState
            }
        }
    }
    
    func updateElement(elementID: ElementID, key: String, value: String, questID: String, location: CLLocationCoordinate2D) async throws {
        let (elementOpt, cached) = try await self.fetchElement(elementID)
        
        guard let element = elementOpt else {
            print("elementOpt == nil")
            return
        }
        
        if cached {
            for element in self.codableState.modify {
                switch (elementID, element.element) {
                case (.way(let wayID), .way(let way)):
                    if way.id == wayID && element.questID == questID {
                        print("Solving quest \(questID) twice on same way \(wayID)")
                        return
                    }
                case (.node(let nodeID), .node(let node)):
                    if node.id == nodeID && element.questID == questID {
                        print("Solving quest \(questID) twice on same node \(nodeID)")
                        return
                    }
                default:
                    continue
                }
            }
        }
        
        let updatedElement = element.updateTag(key, value, self.userInfo, questID, incrementVersion: cached)
        self.codableState.modify.append(QuestWrappedElement(element: updatedElement, location: CodableLocation(lat: location.latitude, lon: location.longitude), questID: questID))
        self.codableState.write(self.userInfo)
        DispatchQueue.main.async {
            self.modify = self.codableState.modify
        }
    }
    
    func submitChangeset() async throws {
        if self.codableState.modify.isEmpty {
            print("Attempted to submit empty changeset...")
            return
        }
        
        let changesetCount = self.codableState.modify.count
        let changesetID = try await self.createChangeset(changesetCount)
        
        let changeset = self.buildChangeset(changesetID)
        let url = OSMUtils.latestVersionedApiUrl("/changeset/\(changesetID)/upload")
        
        print("Submitting changeset to \(url.absoluteString): \(changeset)")
        
        var request = URLRequest(url: url)

        request.httpMethod = "POST"
        request.setValue("Bearer \(self.userInfo.token)", forHTTPHeaderField: "Authorization")
        request.setValue("text/xml", forHTTPHeaderField: "Content-Type")
        request.httpBody = changeset.data(using: .utf8)

        let (data, response) = try await URLSession.shared.data(for: request)

        let httpResponse = (response as? HTTPURLResponse)!

        if httpResponse.statusCode == 409 {
            let error = String(data: data, encoding: .utf8)!
            print("Submitting changeset returned status code \(httpResponse.statusCode), error:", error)
            throw URLError(.badServerResponse, userInfo: ["message": "Recieved status code \(httpResponse.statusCode)", "error": error])
        } else if httpResponse.statusCode != 200 {
            print("Submitting changeset returned status code \(httpResponse.statusCode)")
            throw URLError(.badServerResponse, userInfo: ["message": "Recieved status code \(httpResponse.statusCode)", "changeset": changeset])
        }
        
        print("Submitted changeset")
        let closeUrl = OSMUtils.latestVersionedApiUrl("/changeset/\(changesetID)/close")
        var closeRequest = URLRequest(url: closeUrl)

        closeRequest.httpMethod = "PUT"
        closeRequest.setValue("Bearer \(self.userInfo.token)", forHTTPHeaderField: "Authorization")

        let (closeData, closeResponse) = try await URLSession.shared.data(for: closeRequest)

        let closeHttpResponse = (closeResponse as? HTTPURLResponse)!

        if closeHttpResponse.statusCode == 409 {
            let error = String(data: closeData, encoding: .utf8)!
            print("Closing changeset returned status code \(closeHttpResponse.statusCode), error:", error)
            throw URLError(.badServerResponse, userInfo: ["message": "Recieved status code \(closeHttpResponse.statusCode)", "error": error])
        } else if closeHttpResponse.statusCode != 200 {
            print("Closing changeset returned status code \(closeHttpResponse.statusCode)")
            throw URLError(.badServerResponse, userInfo: ["message": "Recieved status code \(closeHttpResponse.statusCode)"])
        }
        
        print("Closed changeset")
        
        let count = (try await OSMUtils.fetchCompletedQuestCount(self.userInfo.token)) + changesetCount
        try await OSMUtils.setCompletedQuestCount(self.userInfo.token, count)

        self.codableState.modify.removeAll()
        self.codableState.write(self.userInfo)
        DispatchQueue.main.async {
            self.modify = self.codableState.modify
        }
    }
    
    func buildChangeset(_ changesetID: Int) -> String {
        let xml = XML(name: "osmChange", attributes: ["version": String(Self.version), "generator": Self.generator])
            .addChild(
                XML(name: "modify")
                    .addChildren(
                        self.codableState.modify.map({ (element: QuestWrappedElement) in
                            return element.makeChangesetXML(changesetID)
                        })
                    )
            )
        
        return xml.toXMLString()
    }
    
    func reset() {
        self.codableState.modify.removeAll()
        self.codableState.write(self.userInfo)
        DispatchQueue.main.async {
            self.modify = self.codableState.modify
        }
    }
}

struct Node: Identifiable, Codable {
    // Metadata we will use to do operations on nodes, these are NOT included in changeset documents
    var questID: String?
    var uniqueID: String {
        get {
            return "\(self.id)-\(self.questID ?? "")"
        }
    }
    
    // Actual node properties
    var id: Int
    var lat: Double
    var lon: Double
    var version: Int
    var user: String
    var uid: Int
    var visible: Bool
    var changeset: Int
    var timestamp: String
    var tags: [String:String]
    
    init(id: Int, lat: Double, lon: Double, version: Int, user: String, uid: Int, visible: Bool, changeset: Int, timestamp: String, tags: [String : String]) {
        self.id = id
        self.lat = lat
        self.lon = lon
        self.version = version
        self.user = user
        self.uid = uid
        self.visible = visible
        self.changeset = changeset
        self.timestamp = timestamp
        self.tags = tags
    }
}

struct Changeset {
    var modify: [Node]
}
