//
//  OSM.swift
//  OpenStreetQuests
//
//  Created by Elise on 08/01/2023.
//

import Foundation
import CoreLocation

enum ElementID: Equatable {
    case node(Int)
    case way(Int)
}

enum UserDetailsError: String, Error {
    case InvalidStatusCode = "errors.UserDetails.InvalidStatusCode"
    case InvalidResponse = "errors.UserDetails.InvalidResponse"
}

class OSMUtils {
    static func latestVersionedApiUrl(_ path: String) -> URL {
        let path = path.first == "/" ? path : "/\(path)"
        
        return URL(string: OAuth2.isDevelopment ? "https://master.apis.dev.openstreetmap.org/api/0.6\(path)" : "https://api.openstreetmap.org/api/0.6\(path)")!
    }
    
    static func userDetails(_ token: String) async throws -> UserInfo {
        let url = OSMUtils.latestVersionedApiUrl("/user/details.json")
        var request = URLRequest(url: url)
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let (data, response) = try await URLSession.shared.data(for: request)
        let httpResponse = (response as? HTTPURLResponse)!
        
        if httpResponse.statusCode != 200 {
            print("User details returned status code \(httpResponse.statusCode)")
            throw UserDetailsError.InvalidStatusCode
        }
        
        guard let detailsResponse = try? JSONDecoder().decode(UserDetailsResponse.self, from: data) else {
            print("Failed to decode user details response")
            throw UserDetailsError.InvalidResponse
        }
        
        return UserInfo(username: detailsResponse.user.display_name, uid: detailsResponse.user.id, token: token)
    }
    
    static func queryBbox(_ token: String, bbox: (CLLocationCoordinate2D, CLLocationCoordinate2D)) async throws -> OSMQueryResponse {
        let url = OSMUtils.latestVersionedApiUrl("map.json?bbox=\(min(bbox.0.longitude, bbox.1.longitude)),\(min(bbox.0.latitude, bbox.1.latitude)),\(max(bbox.0.longitude, bbox.1.longitude)),\(max(bbox.0.latitude, bbox.1.latitude))")
        var request = URLRequest(url: url)
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let (data, response) = try await URLSession.shared.data(for: request)
        
        let httpResponse = (response as? HTTPURLResponse)!
        
        if httpResponse.statusCode != 200 {
            print("Received bad status code from OSM Query \(httpResponse.statusCode)")
        }
        
        print("Decoding query response")
        
        var val = try JSONDecoder().decode(OSMQueryResponse.self, from: data)
        val.elements = val.elements.filter({ element in
            switch element {
            case .relation:
                return false
            default:
                return true
            }
        })
        return val
    }
    
    static func setCompletedQuestCount(_ token: String, _ count: Int) async throws {
        let url = Self.latestVersionedApiUrl("/user/preferences/osqCompletedQuests")
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.setValue("text/xml", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        request.httpBody = String(count).data(using: .utf8)
        
        let (_, response) = try await URLSession.shared.data(for: request)
        
        let httpResponse = (response as? HTTPURLResponse)!
        
        if httpResponse.statusCode != 200 {
            print("Received bad status code whilst setting completed quest count \(httpResponse.statusCode)")
        }
    }
    
    static func fetchCompletedQuestCount(_ token: String) async throws -> Int {
        let url = Self.latestVersionedApiUrl("/user/preferences/osqCompletedQuests")
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let (data, response) = try await URLSession.shared.data(for: request)
        
        let httpResponse = (response as? HTTPURLResponse)!
        
        if httpResponse.statusCode == 404 {
            try await self.setCompletedQuestCount(token, 0)
            
            return 0
        } else if httpResponse.statusCode != 200 {
            print("Received bad status code whilst setting completed quest count \(httpResponse.statusCode)")
        }
        
        
        if let strData = String(data: data, encoding: .utf8), let count = Int(strData) {
            return count
        } else {
            print("Failed to decode completed quest count...")
            return 0
        }
    }
}

struct UserDetailsResponse: Decodable {
    let user: UserDetails
}

struct UserDetails: Decodable {
    let id: Int
    let display_name: String
}
