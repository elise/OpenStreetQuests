//
//  Location.swift
//  OpenStreetQuests
//
//  Created by Elise on 09/01/2023.
//

import Foundation
import CoreLocation
import SwiftyXML

enum OSMCodableElement: Codable {
    case node(OSMNode)
    case way(OSMWay)
    
    func elementID() -> ElementID {
        switch self {
        case .way(let way):
            return .way(way.id)
        case .node(let node):
            return .node(node.id)
        }
    }
    
    func tagPresent(_ key: String) -> Bool {
        switch self {
        case .way(let way):
            return way.tags?[key] != nil
        case .node(let node):
            return node.tags?[key] != nil
        }
    }
    
    mutating func tagRemove(_ key: String) {
        switch self {
        case .way(var way):
            way.tags?.removeValue(forKey: key)
            self = .way(way)
        case .node(var node):
            node.tags?.removeValue(forKey: key)
            self = .node(node)
        }
    }
    
     mutating func tagUpdate(_ key: String, _ value: String) {
        switch self {
        case .way(var way):
            if way.tags == nil {
                way.tags = [:]
            }
            way.tags?[key] = value
            self = .way(way)
        case .node(var node):
            if node.tags == nil {
                node.tags = [:]
            }
            node.tags?[key] = value
            self = .node(node)
        }
    }
    
    func updateTag(_ key: String, _ value: String, _ userInfo: UserInfo, _ questID: String, incrementVersion: Bool = false) -> Self {
        switch self {
        case .way(var way):
            if way.tags == nil {
                way.tags = [:]
            }
            way.tags?[key] = value
            way.timestamp = ISO8601DateFormatter().string(from: Date())
            way.user = userInfo.username
            way.uid = userInfo.uid
            
            if incrementVersion {
                way.version += 1
            }
            
            return .way(way)
        case .node(var node):
            if node.tags == nil {
                node.tags = [:]
            }
            node.tags?[key] = value
            node.timestamp = ISO8601DateFormatter().string(from: Date())
            node.user = userInfo.username
            node.uid = userInfo.uid
            
            if incrementVersion {
                node.version += 1
            }
            
            return .node(node)
        }
    }
}

enum OSMAPIElement: Decodable {
    case node(OSMNode)
    case way(OSMWay)
    case relation//(OSMQueryResponseRelation)
    
    enum TypeCodingKey: String, CodingKey {
        case type = "type"
    }
    
    enum ElementType: String, Decodable {
        case node = "node"
        case way = "way"
        case relation = "relation"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: TypeCodingKey.self)
        guard let type = try? container.decode(ElementType.self, forKey: .type) else {
            throw DecodingError.typeMismatch(TypeCodingKey.self, DecodingError.Context.init(codingPath: container.codingPath, debugDescription: "Missing type key in API response.", underlyingError: nil))
        }
        
        let objectContainer = try decoder.singleValueContainer()
        
        switch type {
        case .node:
            let node = try objectContainer.decode(OSMNode.self)
            self = .node(node)
        case .way:
            let way = try objectContainer.decode(OSMWay.self)
            self = .way(way)
        case .relation:
            self = .relation
//            let relation = try objectContainer.decode(OSMQueryResponseRelation.self)
//            self = .relation(relation)
        }
    }
    
    func intoCodable() -> OSMCodableElement {
        switch self {
        case .way(let way):
            return OSMCodableElement.way(way)
        case .node(let node):
            return OSMCodableElement.node(node)
        case .relation:
            fatalError("found relation")
        }
    }
    
    static func fetch(_ id: ElementID) async throws -> Self? {
        switch id {
        case .node(let nodeID):
            guard let node = try await OSMNode.fetch(nodeID) else { return nil }
            return .node(node)
        case .way(let wayID):
            guard let way = try await OSMWay.fetch(wayID) else { return nil }
            return .way(way)
        }
    }
}

struct OSMNode: Codable {
    var id: Int
    var lat: Double
    var lon: Double
    var timestamp: String?
    var version: Int
    var changeset: Int?
    var user: String
    var uid: Int
    var visible: Bool?
    var tags: [String:String]?
    
    static func fetch(_ id: Int) async throws -> OSMNode? {
        let url = OSMUtils.latestVersionedApiUrl("/node/\(id)")
        
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let (data, response) = try await URLSession.shared.data(for: request)
        
        let httpResponse = (response as? HTTPURLResponse)!
        
        if httpResponse.statusCode != 200 {
            print("Fetching node \(id) returned status code \(httpResponse.statusCode)")
            return nil
        }
        
        let elements = try JSONDecoder().decode(OSMQueryResponse.self, from: data).elements
        
        switch elements[0] {
        case .node(let node):
            return node
        default:
            fatalError("non-node when fetching individual node")
        }
    }
    
    func updateTag(_ key: String, _ value: String, _ userInfo: UserInfo, _ questID: String, incrementVersion: Bool = false) -> Self {
        var copy = self
        
        if copy.tags == nil {
            copy.tags = [:]
        }
        
        copy.tags?[key] = value
        copy.timestamp = ISO8601DateFormatter().string(from: Date())
        copy.user = userInfo.username
        copy.uid = userInfo.uid
        
        if incrementVersion {
            copy.version += 1
        }
        
        return copy
    }
    
    func intoElement() -> OSMCodableElement? {
        return .node(self)
    }
}

//struct OSMQueryResponseRelation: Decodable {
//    let id: Int
//    let timestamp: String
//    let version: Int
//    let changeset: Int
//    let user: String
//    let uid: Int
//    let visible: Bool?
//    let members: [OSMQueryResponseRelationMember]
//    let tags: [String:String]?
//}
//
//struct OSMQueryResponseRelationMember: Decodable {
//    let type: String
//    let ref: Int
//    let role: String
//}

struct OSMWay: Codable {
    let id: Int
    var timestamp: String?
    var version: Int
    var changeset: Int?
    var user: String
    var uid: Int
    let nodes: [Int]
    let visible: Bool?
    var tags: [String:String]?
    
    static func fetch(_ id: Int) async throws -> OSMWay? {
        let url = OSMUtils.latestVersionedApiUrl("/way/\(id)")

        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let (data, response) = try await URLSession.shared.data(for: request)
        
        let httpResponse = (response as? HTTPURLResponse)!
        
        if httpResponse.statusCode != 200 {
            print("Fetching way \(id) returned status code \(httpResponse.statusCode)")
            return nil
        }
        
        let elements = try JSONDecoder().decode(OSMQueryResponse.self, from: data).elements
        
        switch elements[0] {
        case .way(let way):
            return way
        default:
            fatalError("non-way when fetching individual way")
        }
    }
    
    func intoElement() -> OSMCodableElement? {
        return .way(self)
    }
    
    func nodes(bboxContents: [OSMCodableElement]) -> [OSMNode]? {
        var nodes: [OSMNode] = []
        for nodeID in self.nodes {
            let node = bboxContents.first { element in
                element.elementID() == .node(nodeID)
            }
            
            switch node {
            case .node(let node):
                nodes.append(node)
            default:
                continue
            }
        }
        
        if nodes.count != self.nodes.count {
            print("Way \(self.id) has \(self.nodes.count) nodes but the bbox only contained \(nodes.count), skipping way")
            return nil
        }
        
        return nodes
    }
    
    func centerLocation(_ nodes: [OSMNode]) -> CLLocationCoordinate2D {
        var lowestLat = nodes[0].lat
        var highestLat = nodes[0].lat
        var lowestLon = nodes[0].lon
        var highestLon = nodes[0].lon
        
        for node in nodes {
            if node.lat < lowestLat {
                lowestLat = node.lat
            }
            
            if node.lat > highestLat {
                highestLat = node.lat
            }
            
            if node.lon < lowestLon {
                lowestLon = node.lon
            }
            
            if node.lon > highestLon {
                highestLon = node.lon
            }
        }
        
        let centerLon = lowestLon + ((highestLon - lowestLon) / 2)
        let centerLat = lowestLat + ((highestLat - lowestLat) / 2)
        
        return CLLocationCoordinate2D(latitude: centerLat, longitude: centerLon)
    }
}

struct OSMQueryResponse: Decodable {
    var elements: [OSMAPIElement]
}

struct CodableLocation: Codable {
    let lat: Double
    let lon: Double
}

struct QuestWrappedElement: Identifiable, Codable {
    var element: OSMCodableElement
    
    var location: CodableLocation
    var questID: String?
    var id: String {
        get {
            let type: String
            let id: Int
            switch self.element {
            case .node(let node):
                type = "node"
                id = node.id
            case .way(let way):
                type = "way"
                id = way.id
            }
            return "\(type)-\(id)-\(self.questID ?? "")"
        }
    }
    
    func matchesID(_ elementID: ElementID) -> Bool {
        switch (self.element, elementID) {
        case (.node(let node), .node(let nodeID)):
            return node.id == nodeID
        case (.way(let way), .way(let wayID)):
            return way.id == wayID
        default:
            return false
        }
    }
    
    func makeChangesetXML(_ changesetID: Int) -> XML {
        switch self.element {
        case .node(let node):
            var attributes: [String: Any] = [
                "id": node.id,
                "lat": node.lat,
                "lon": node.lon,
                "user": node.user,
                "uid": node.uid,
                "version": node.version,
                "changeset": changesetID,
                "timestamp": node.timestamp!
            ]
            
            if let visible = node.visible {
                attributes["visible"] = visible
            }
            
            return XML(
                name: "node",
                attributes: attributes
            ).addChildren(
                (node.tags ?? [:]).map({ (key: String, value: String) in
                    return XML(
                        name: "tag",
                        attributes: [
                            "k": key,
                            "v": value
                        ]
                    )
                })
            )
        case .way(let way):
            var attributes: [String : Any] = [
                "id": way.id,
                "user": way.user,
                "uid": way.uid,
                "version": way.version,
                "changeset": changesetID,
                "timestamp": way.timestamp!
            ]
            
            if let visible = way.visible {
                attributes["visible"] = visible
            }
            
            return XML(
                name: "way",
                attributes: attributes
            ).addChildren(
                way.nodes.map({ nodeID in
                    return XML(
                        name: "nd",
                        attributes: [
                            "ref": nodeID
                        ]
                    )
                })
            ).addChildren(
                (way.tags ?? [:]).map({ (key: String, value: String) in
                    return XML(
                        name: "tag",
                        attributes: [
                            "k": key,
                            "v": value
                        ]
                    )
                })
            )
        }
    }
}
