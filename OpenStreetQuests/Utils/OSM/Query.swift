//
//  Query.swift
//  OpenStreetQuests
//
//  Created by Elise on 09/01/2023.
//

import Foundation
import CoreLocation

enum OSMQueryElement: String, Decodable {
    case node = "node"
    case way = "way"
    case relation = "relation"
}

struct NonMatchingQueryElement: Decodable {
    var key: String
    var value: String
    
    init(_ key: String, _ value: String) {
        self.key = key
        self.value = value
    }
    
    init(from decoder: Decoder) throws {
        let stringValue = try decoder.singleValueContainer().decode(String.self)
        
        let splitString = stringValue.split(separator: "!=")
        
        if splitString.count != 2 {
            fatalError("Decoding non-matching query element in quest did not contain a count of 2 after splitting")
        }
        
        self.init(String(splitString[0]), String(splitString[1]))
    }
}

/// Queries are executed in the following order: `absentTags`, `presentTags`, `nonMatchingTags`, `matchingTags`
struct OSMQuery: Decodable {
    let elementType: OSMQueryElement
    /// Values in here must be present as keys, with any non-nil value
    let presentTags: [String]
    /// Values in here must not be present as keys in the element
    let absentTags: [String]
    /// For entries in here, a node has to exactly contain `key=value`
    let matchingTags: [String:String]
    /// Entries are expected  as strings in format `key!=value`. For entries in here, a node has to not exactly contain `key=value` (so `key=otherValue` or `key=nil`are both ok).
    let nonMatchingTags: [NonMatchingQueryElement]
    
    static func sendQueries(_ userInfo: UserInfo, _ bbox: (CLLocationCoordinate2D, CLLocationCoordinate2D)) async throws -> ([String:[String: [OSMCodableElement]]], [OSMCodableElement]) {
        let bboxContents = (try await OSMUtils.queryBbox(userInfo.token, bbox: bbox)).elements.map { element in
            element.intoCodable()
        }
        
        let inactiveQuests = InactiveQuestList.read(userInfo)
        
        var out: [String:[String:[OSMCodableElement]]] = [:]
        
        for (questID, quest) in Quest.quests {
            if inactiveQuests.questIDs.firstIndex(of: questID) != nil {
                continue
            }

            var questOut: [String:[OSMCodableElement]] = [:]
            for (qSKey, querySolution) in quest.querySolution {
                var qSOut: [OSMCodableElement] = []
                let query = querySolution.query
                
                for element in bboxContents {
                    switch element {
                    case .node(let node):
                        var valid = true
                        if query.elementType != .node {
                            continue
                        }
                        
                        for tag in query.nonMatchingTags {
                            if node.tags?[tag.key] == tag.value {
                                valid = false
                                break
                            }
                        }
                        
                        if !valid {
                            continue
                        }
                        
                        for absentTag in query.absentTags {
                            if node.tags?.index(forKey: absentTag) != nil {
                                valid = false
                                break
                            }
                        }
                        
                        if !valid {
                            continue
                        }
                        
                        for (key, value) in query.matchingTags {
                            if node.tags?[key] != value {
                                valid = false
                                break
                            }
                        }
                        
                        if !valid {
                            continue
                        }
                        
                        qSOut.append(.node(node))
                    case .way(let way):
                        var valid = true
                        if query.elementType != .way {
                            continue
                        }
                        
                        for absentTag in query.absentTags {
                            if way.tags?.index(forKey: absentTag) != nil {
                                valid = false
                                break
                            }
                        }
                        
                        if !valid {
                            continue
                        }
                        
                        for tag in query.nonMatchingTags {
                            if way.tags?[tag.key] == tag.value {
                                valid = false
                                break
                            }
                        }
                        
                        if !valid {
                            continue
                        }
                        
                        for (key, value) in query.matchingTags {
                            if way.tags?[key] != value {
                                valid = false
                                break
                            }
                        }
                        
                        if !valid {
                            continue
                        }
                        
                        qSOut.append(.way(way))
                    }
                    questOut[qSKey] = qSOut
                }
                out[questID] = questOut
            }
        }
        
        return (out, bboxContents)
    }
}


