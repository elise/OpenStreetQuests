//
//  SystemLocation.swift
//  OpenStreetQuests
//
//  Created by Elise on 03/01/2023.
//

import Foundation
import CoreLocation

class LocationHelper: NSObject, ObservableObject, CLLocationManagerDelegate {
    private let locationManager: CLLocationManager = CLLocationManager()
    @Published var authorisationStatus: CLAuthorizationStatus = .notDetermined
    @Published var location: CLLocation?
    
    var locationUpdateCallback: ((CLLocation) -> Void)?
    
    override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        self.authorisationStatus = manager.authorizationStatus
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        self.location = location
        if let callback = self.locationUpdateCallback {
            callback(location)
        }
    }
}

extension CLAuthorizationStatus {
    var asString: String {
        get {
            switch self {
            case .restricted:
                return "Restricted"
            case .authorizedAlways:
                return "Authorised"
            case .authorizedWhenInUse:
                return "Authorised"
            case .denied:
                return "Denied"
            case .notDetermined:
                return "Indetermined"
            case let x:
                return "Unknown Value (" + String(x.rawValue) + ")"
            }
        }
    }
}
