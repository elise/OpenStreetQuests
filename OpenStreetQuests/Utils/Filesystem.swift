//
//  Filesystem.swift
//  OpenStreetQuests
//
//  Created by Elise on 08/01/2023.
//

import Foundation

class FilesystemUtils {
    static func documentURL(subpath: String) -> URL {
        let documentsUrl: URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        return documentsUrl.appending(path: subpath)
    }
    
    static func readJSON<T: Decodable>(path: URL, removeWhenInvalid: Bool) -> T? {
        guard let jsonData = try? String(contentsOfFile: path.path(percentEncoded: false)).data(using: .utf8) else {
            return nil
        }

        do {
            let decodedData = try JSONDecoder().decode(T.self, from: jsonData)
            return decodedData
        } catch let error {
            print("Failed to decode JSON:", error)

            if removeWhenInvalid {
                try? FileManager.default.removeItem(at: path)
            }
            return nil
        }
    }

    static func writeJSON<T: Encodable>(path: URL, value: T) {
        do {
            let encoded = try JSONEncoder().encode(value)
            try encoded.write(to: path)
        } catch let error {
            print("Failed to write JSON:", error)
        }
    }
}
