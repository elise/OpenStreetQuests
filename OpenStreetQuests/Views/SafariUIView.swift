//
//  SafariUIView.swift
//  OpenStreetQuests
//
//  Created by Elise on 17/01/2023.
//

import SwiftUI
import SafariServices

struct SafariUIView: UIViewControllerRepresentable {
    func makeUIViewController(context: Context) -> SFSafariViewController {
        return SFSafariViewController(url: OAuth2.authenticateUrl())
    }
    
    func updateUIViewController(_ uiViewController: SFSafariViewController, context: Context) {
        
    }
}
