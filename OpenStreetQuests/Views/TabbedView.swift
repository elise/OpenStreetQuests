//
//  TabbedView.swift
//  OpenStreetQuests
//
//  Created by Elise on 09/01/2023.
//

import SwiftUI

struct TabbedView: View {
    @State var changesetState: CurrentChangeset
    let userInfo: UserInfo
    @EnvironmentObject var locationHelper: LocationHelper
    @State var mapViewState: MapViewState
    @State var tabSelection = 0
    
    let logoutCallback: () -> Void
    
    init(userInfo: UserInfo, _ logoutCallback: @escaping () -> Void) {
        self.changesetState = CurrentChangeset(userInfo: userInfo)
        self.userInfo = userInfo
        self.mapViewState = MapViewState(userInfo)
        self.logoutCallback = logoutCallback
    }
    
    var body: some View {
        TabView(selection: self.$tabSelection) {
            QuestMapStackView(self.mapViewState)
                .environmentObject(self.changesetState)
                .environmentObject(self.locationHelper)
                .tabItem {
                    Label("tabs.map", systemImage: "mappin")
                }
                .tag(0)
            ChangesetView(self.mapViewState, self.$tabSelection)
                .environmentObject(self.changesetState)
                .tabItem {
                    Label("tabs.changeset", systemImage: "list.star")
                }
                .tag(1)
            SettingsView(userInfo: self.userInfo, logoutCallback: logoutCallback)
                .tabItem {
                    Label("tabs.settings", systemImage: "gear")
                }
                .tag(2)
        }
    }
}
