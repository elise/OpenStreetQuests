//
//  QuestManagementView.swift
//  OpenStreetQuests
//
//  Created by Elise on 22/01/2023.
//

import SwiftUI

struct QuestManagementView: View {
    @State var inactiveQuestList: InactiveQuestList
    let userInfo: UserInfo
    var exit: () -> Void
    
    init(_ userInfo: UserInfo, _ exit: @escaping () -> Void) {
        self.userInfo = userInfo
        self.inactiveQuestList = InactiveQuestList.read(userInfo)
        self.exit = exit
    }
    
    var body: some View {
        VStack(spacing: 0.0) {
            HStack {
                Text("questManagement.quests")
                    .padding(EdgeInsets(top: 18.0, leading: 18.0, bottom: 18.0, trailing: 0.0))
                    .bold()
                    .foregroundColor(.accentColor)
                    .dynamicTypeSize(.xxLarge)
                Text("(\(Quest.quests.count - self.inactiveQuestList.questIDs.count)/\(Quest.quests.count))")
                    .padding(EdgeInsets(top: 18.0, leading: 0.0, bottom: 18.0, trailing: 18.0))
                    .bold()
                    .foregroundColor(.accentColor)
                    .dynamicTypeSize(.xxLarge)
                Spacer()
                Button(action: {
                    self.exit()
                }, label: {
                    Text("questManagement.back")
                        .padding()
                        .bold()
                        .foregroundColor(.accentColor)
                        .dynamicTypeSize(.xxLarge)
                })
            }
            List(Quest.quests.sorted(by: { leftKV, rightKV in
                if leftKV.value.importance == rightKV.value.importance {
                    return leftKV.key > rightKV.key
                } else {
                    return leftKV.value.importance > rightKV.value.importance
                }
            }), id: \.0) { (key, value) in
                VStack(spacing: 0.0) {
                    HStack(spacing: 0.0) {
                        VStack(spacing: 0.0) {
                            HStack {
                                Text(LocalizedStringKey(value.name))
                                    .bold()
                                    .multilineTextAlignment(.leading)
                                Spacer()
                            }
                            HStack(spacing: 0.0) {
                                Text(LocalizedStringKey(value.description)).multilineTextAlignment(.leading)
                                    .dynamicTypeSize(.xSmall)
                                Spacer()
                            }
                        }
                        Spacer()
                        Button(action: {
                            if let index = self.inactiveQuestList.questIDs.firstIndex(of: key) {
                                withAnimation(.easeIn(duration: 0.1)) {
                                    self.inactiveQuestList.questIDs.remove(at: index)
                                    let _ = self.inactiveQuestList.write(self.userInfo)
                                }
                            } else {
                                withAnimation(.easeIn(duration: 0.1)) {
                                    self.inactiveQuestList.questIDs.append(key)
                                    let _  = self.inactiveQuestList.write(self.userInfo)
                                }
                            }
                        }, label: {
                            if self.inactiveQuestList.questIDs.firstIndex(of: key) == nil {
                                ZStack {
                                    RoundedRectangle(
                                        cornerSize: CGSize(width: 6.0, height: 6.0),
                                        style: .continuous
                                    )
                                    Image(systemName: "checkmark")
                                        .foregroundColor(Color.white)
                                }
                                .frame(width: 24.0, height: 24.0)
                            } else {
                                RoundedRectangle(
                                    cornerSize: CGSize(width: 6.0, height: 6.0),
                                    style: .continuous
                                )
                                .stroke(Color.accentColor, lineWidth: 4.0)
                                .frame(width: 24.0, height: 24.0)
                            }
                        })
                    }
                }
            }
        }
    }
}

struct QuestManagementView_Previews: PreviewProvider {
    static var previews: some View {
        QuestManagementView(UserInfo(username: "test", uid: 0, token: ""), {})
    }
}
