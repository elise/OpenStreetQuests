//
//  SelectionQuestView.swift
//  OpenStreetQuests
//
//  Created by Elise on 14/01/2023.
//

import SwiftUI
import CoreLocation

struct SelectionQuestGroup: Identifiable, Decodable {
    let name: String
    let entries: [SelectionQuestEntry]
    var id: String {
        get {
            return name
        }
    }
}

struct SelectionQuestEntry: Identifiable, Decodable, Equatable {
    let display: String
    let description: String
    let tagValue: String
    
    var id: String {
        get {
            return "\(display)-\(tagValue)"
        }
    }
}

struct SelectionEntryView: View {
    let display: String
    let description: String
    
    var body: some View {
        VStack(spacing: 0) {
            Text(LocalizedStringKey(stringLiteral: self.display))
                .foregroundColor(.accentColor)
                .padding(EdgeInsets(top: 9.0, leading: 18.0, bottom: 2.0, trailing: 18.0))
                .bold(true)
                .dynamicTypeSize(.large)
            Text(LocalizedStringKey(stringLiteral: self.description))
                .foregroundColor(.accentColor)
                .padding(EdgeInsets(top: 2.0, leading: 18.0, bottom: 9.0, trailing: 18.0))
                .dynamicTypeSize(.medium)
        }
    }
}

struct SelectionQuestView: View {
    @EnvironmentObject var changeset: CurrentChangeset
    @EnvironmentObject var mapState: MapViewState

    let elementID: ElementID
    let destinationKey: String
    
    let questID: String
    let questName: String
    let question: String
    
    let entries: [String:[SelectionQuestEntry]]
    @State var selection: SelectionQuestEntry?
    
    let elementName: String?
    let location: CLLocationCoordinate2D
    
    let exit: () -> Void
    @State var exited: Bool = false
    
    var body: some View {
        VStack(spacing: 0) {
            Text(LocalizedStringKey(stringLiteral: questName))
                .bold()
                .dynamicTypeSize(.xxxLarge)
                .foregroundColor(.accentColor)
            Text(LocalizedStringKey(stringLiteral: question))
                .dynamicTypeSize(.large)
                .padding(EdgeInsets(
                    top: 9.0,
                    leading: 18.0,
                    bottom: self.elementName == nil ? 18.0 : 0.0,
                    trailing: 18.0
                ))
                .foregroundColor(.accentColor)
            if let elementName = self.elementName {
                Text(LocalizedStringKey(stringLiteral: elementName))
                    .foregroundColor(.accentColor)
                    .italic()
                    .dynamicTypeSize(.large)
                    .padding(EdgeInsets(top: 4.0, leading: 18.0, bottom: 18.0, trailing: 18.0))
            }
            VStack {
                ScrollView {
                    ForEach(self.entries.keys.sorted(), id: \.self) { key in
                        let entries = self.entries[key]!
                        if key == "" {
                            ForEach(entries.sorted(by: { leftSide, rightSide in
                                return leftSide.id < rightSide.id
                            }).indices, id: \.self) { entryIdx in
                                let entry = entries[entryIdx]
                                Button(action: {
                                    if !self.exited {
                                        DispatchQueue.main.async {
                                            self.selection = entry
                                        }
                                    }
                                }, label: {
                                    SelectionEntryView(display: entry.display, description: entry.description)
                                })
                                .disabled(self.selection?.id == entry.id)
                                .overlay(
                                    self.selection == entry ? RoundedRectangle(cornerRadius: 20)
                                        .stroke(.primary, lineWidth: 2).foregroundColor(.accentColor) : nil
                                )
                            }
                        } else {
                            DisclosureGroup(LocalizedStringKey(stringLiteral: key)) {
                                ForEach(entries.sorted(by: { leftSide, rightSide in
                                    return leftSide.id < rightSide.id
                                }).indices, id: \.self) { entryIdx in
                                    let entry = entries[entryIdx]
                                    Button(action: {
                                        if !self.exited {
                                            DispatchQueue.main.async {
                                                self.selection = entry
                                            }
                                        }
                                    }, label: {
                                        SelectionEntryView(display: entry.display, description: entry.description)
                                    })
                                    .disabled(self.selection?.id == entry.id)
                                    .overlay(
                                        self.selection == entry ? RoundedRectangle(cornerRadius: 20)
                                            .stroke(.primary, lineWidth: 2).foregroundColor(.accentColor) : nil
                                    )
                                }
                            }
                        }
                    }
                }.frame(maxHeight: 225)
                Button(action: {
                    if !self.exited, let selected = self.selection {
                        Task {
                            do {
                                try await self.changeset.updateElement(elementID: self.elementID, key: self.destinationKey, value: selected.tagValue, questID: self.questID, location: self.location)
                            } catch let error {
                                print("Error whilst updating node", error)
                            }
                        }
                        mapState.questPointsMapLayer.removeQuest(elementID: self.elementID, questID: self.questID)
                    }
                    self.exited = true
                    self.exit()
                }, label: {
                    Spacer()
                    Text("quests.count.submit")
                    Spacer()
                })
                .disabled(self.selection == nil)
                .padding(EdgeInsets(top: 18.0, leading: 0.0, bottom: 18.0, trailing: 0.0))
                .overlay(
                    RoundedRectangle(cornerRadius: 20)
                        .stroke(.primary, lineWidth: 2)
                        .foregroundColor(.accentColor)
                )
                .padding(EdgeInsets(top: 0.0, leading: 18.0, bottom: 0.0, trailing: 18.0))
                Button(action: {
                    self.exited = true
                    self.exit()
                }, label: {
                    Spacer()
                    Text("quests.count.exit")
                    Spacer()
                })
                .padding(EdgeInsets(top: 18.0, leading: 0.0, bottom: 18.0, trailing: 0.0))
                .overlay(
                    RoundedRectangle(cornerRadius: 20)
                        .stroke(.primary, lineWidth: 2)
                        .foregroundColor(.accentColor)
                )
                .padding(EdgeInsets(top: 0.0, leading: 18.0, bottom: 0.0, trailing: 18.0))
            }
        }.padding().task {
            while true {
                do {
                    try await Task.sleep(for: Duration.seconds(1))
                } catch _ {
                    return
                }
                
                if self.exited {
                    return
                }
                
                self.mapState.rerender()
            }
        }
    }
}
