//
//  CountQuestView.swift
//  OpenStreetQuests
//
//  Created by Elise on 11/01/2023.
//

import SwiftUI
import CoreLocation

struct CountQuestView: View {
    @EnvironmentObject var changeset: CurrentChangeset
    @EnvironmentObject var mapState: MapViewState
    
    let elementID: ElementID
    let key: String
    
    let questID: String
    let questName: String
    let question: String
    
    let elementName: String?
    let location: CLLocationCoordinate2D
    
    @State var exited: Bool = false
    var exit: () -> Void
    
    
    @State var countStr: String = "" {
        didSet {
            let filtered = self.countStr.filter({ ch in
                return ch.isNumber
            })
            
            if self.countStr != filtered {
                self.countStr = filtered
            }
        }
    }
    
    var body: some View {
        VStack(spacing: 0) {
            Text(LocalizedStringKey(stringLiteral: questName))
                .bold()
                .dynamicTypeSize(.xxxLarge)
                .foregroundColor(.accentColor)
            Text(LocalizedStringKey(stringLiteral: question))
                .dynamicTypeSize(.large)
                .padding(EdgeInsets(
                    top: 9.0,
                    leading: 18.0,
                    bottom: self.elementName == nil ? 18.0 : 0.0,
                    trailing: 18.0
                ))
                .foregroundColor(.accentColor)
            if let elementName = self.elementName {
                Text(LocalizedStringKey(stringLiteral: elementName))
                    .foregroundColor(.accentColor)
                    .italic()
                    .dynamicTypeSize(.large)
                    .padding(EdgeInsets(
                        top: 4.0,
                        leading: 18.0,
                        bottom: 18.0,
                        trailing: 18.0
                    ))
            }
            VStack {
                HStack {
                    TextField(LocalizedStringKey("quests.count.textField") , text: self.$countStr)
                        .keyboardType(.decimalPad)
                        .padding(EdgeInsets(top: 18.0, leading: 18.0, bottom: 18.0, trailing: 2.0))
                        .foregroundColor(.accentColor)
                    Button(action: {
                        if !self.exited, let parsedInt = Int(self.countStr) {
                            DispatchQueue.main.async {
                                Task {
                                    do {
                                        try await self.changeset.updateElement(elementID: self.elementID, key: self.key, value: String(parsedInt), questID: self.questID, location: self.location)
                                    } catch let error {
                                        print("Error whilst updating node", error)
                                    }
                                }
                            }
                            mapState.questPointsMapLayer.removeQuest(elementID: self.elementID, questID: self.questID)
                            self.exited = true
                            exit()
                        }
                    }, label: {
                        Spacer()
                        Text("quests.count.submit")
                        Spacer()
                    })
                    .disabled(self.exited || self.countStr.isEmpty)
                    .padding(EdgeInsets(top: 18.0, leading: 0.0, bottom: 18.0, trailing: 0.0))
                    .overlay(
                        RoundedRectangle(cornerRadius: 20)
                            .stroke(.primary, lineWidth: 2)
                            .foregroundColor(.accentColor)
                    )
                    .padding(EdgeInsets(top: 0.0, leading: 2.0, bottom: 0.0, trailing: 18.0))
                }
                Button(action: {
                    self.exited = true
                    self.exit()
                }, label: {
                    Spacer()
                    Text("quests.count.exit")
                    Spacer()
                })
                .padding(EdgeInsets(top: 18.0, leading: 0.0, bottom: 18.0, trailing: 0.0))
                .overlay(
                    RoundedRectangle(cornerRadius: 20)
                        .stroke(.primary, lineWidth: 2)
                        .foregroundColor(.accentColor)
                )
                .padding(EdgeInsets(top: 0.0, leading: 18.0, bottom: 0.0, trailing: 18.0))
            }
        }.padding().task {
            while true {
                do {
                    try await Task.sleep(for: Duration.seconds(1))
                } catch _ {
                    return
                }
                
                if self.exited {
                    return
                }
                
                self.mapState.rerender()
            }
        }
    }
}
