//
//  SettingsView.swift
//  OpenStreetQuests
//
//  Created by Elise on 10/01/2023.
//

import SwiftUI

struct SettingsView: View {
    let userInfo: UserInfo
    let logoutCallback: () -> Void
    
    @State var questManagement: Bool = false
    @State var questsCompleted = 0
    
    var body: some View {
        VStack {
            HStack {
                Text("settings.settings")
                    .padding()
                    .bold()
                    .foregroundColor(.accentColor)
                    .dynamicTypeSize(.xxLarge)
                Spacer()
            }
            HStack {
                Text("settings.questsCompleted")
                    .bold()
                    .foregroundColor(.accentColor)
                    .padding()
                Spacer()
                Text(String(self.questsCompleted))
                    .bold()
                    .foregroundColor(.accentColor)
                    .padding()
            }
            Button(action: {
                DispatchQueue.main.async {
                    self.questManagement = true
                }
            }, label: {
                Text("settings.questManagement")
                    .bold()
                    .foregroundColor(.accentColor)
                    .padding()
            })
            HStack {
                HStack {
                    Text("settings.username")
                        .bold()
                        .foregroundColor(.accentColor)
                    Text(userInfo.username)
                        .bold()
                        .foregroundColor(.accentColor)
                }.padding()
                Spacer()
                Button(action: {
                    UserInfo.clear()
                    self.logoutCallback()
                }, label: {
                    Text("settings.logout")
                        .bold()
                        .foregroundColor(.accentColor)
                }).padding()
            }
            Link(
                "settings.source",
                destination: URL(string: "https://gitlab.com/elise/OpenStreetQuests")!
            )
            .bold()
            .foregroundColor(.accentColor)
            .padding()
            Link(
                "settings.privacyPolicy",
                destination: URL(string: "https://gitlab.com/elise/OpenStreetQuests/-/blob/main/PRIVACY.md")!
            )
            .bold()
            .foregroundColor(.accentColor)
            .padding()
            Link(
                "settings.osmCopyright",
                destination: URL(string: "https://www.openstreetmap.org/copyright/")!
            )
            .bold()
            .foregroundColor(.accentColor)
            .padding()
            Spacer()
            VStack {
                Text(verbatim: "OpenStreetQuests v\(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "???")")
                    .dynamicTypeSize(DynamicTypeSize.medium)
                    .foregroundColor(.accentColor)
                HStack {
                    Spacer()
                    Text("Made with").dynamicTypeSize(DynamicTypeSize.xSmall).foregroundColor(.accentColor)
                    Image(systemName: "heart.fill")
                        .resizable()
                        .foregroundColor(.red)
                        .frame(width: 15, height: 15, alignment: .trailing)
                    Spacer()
                }
            }.padding()
        }.task {
            do {
                self.questsCompleted = try await OSMUtils.fetchCompletedQuestCount(self.userInfo.token)
            } catch let error {
                print("Failed to fetch completed quests:", error)
            }
        }.sheet(isPresented: self.$questManagement, content: {
            QuestManagementView(self.userInfo, {
                DispatchQueue.main.async {
                    self.questManagement = false
                }
            })
        })
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView(userInfo: UserInfo(username: "test", uid: 0, token: ""), logoutCallback: {})
    }
}
