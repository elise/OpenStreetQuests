//
//  LoginView.swift
//  OpenStreetQuests
//
//  Created by Elise on 08/01/2023.
//

import SwiftUI
import SafariServices

struct LoginView: View {
    @Binding var error: String?
    @Binding var safariViewPresented: Bool
    
    var body: some View {
        VStack {
            HStack {
                Text("OpenStreetQuests")
                    .dynamicTypeSize(.xLarge)
                    .bold()
                    .padding()
                    .foregroundColor(.accentColor)
                Spacer()
            }
            Spacer()
            if let error = self.error {
                Text(error)
                    .bold()
                    .padding()
                    .dynamicTypeSize(.large)
                    .foregroundColor(.red)
            }
            Button(action: {
                self.safariViewPresented = true
            }, label: {
                Text("login.loginWithOSMButton")
            })
            Spacer()
            HStack {
                Link(
                    "login.source",
                    destination: URL(string: "https://gitlab.com/elise/OpenStreetQuests")!
                )
                .bold()
                .foregroundColor(.accentColor)
                .padding()
                Link(
                    "login.privacyPolicy",
                    destination: URL(string: "https://gitlab.com/elise/OpenStreetQuests/-/blob/main/PRIVACY.md")!
                )
                .bold()
                .foregroundColor(.accentColor)
                .padding()
            }
            HStack {
                Spacer()
                Text("Made with")
                    .bold()
                    .foregroundColor(.accentColor)
                Image(systemName: "heart.fill")
                    .resizable()
                    .foregroundColor(.red)
                    .frame(width: 18, height: 18, alignment: .trailing)
                Spacer()
            }.padding()
        }.sheet(isPresented: self.$safariViewPresented, content: {
            SafariUIView()
        })
    }
}

struct LoginView_Previews: PreviewProvider {
    @State static var error: String?
    @State static var safariViewPresented: Bool = false
    static var previews: some View {
        LoginView(error: Self.$error, safariViewPresented: Self.$safariViewPresented)
    }
}
