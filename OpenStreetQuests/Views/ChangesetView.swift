//
//  ChangesetView.swift
//  OpenStreetQuests
//
//  Created by Elise on 07/01/2023.
//

import SwiftUI
import TangramMap

struct ChangesetView: View {
    @EnvironmentObject var changeset: CurrentChangeset
    @State var confirmReset: Bool = false
    @State var confirmCommit: Bool = false
    @State var confirmDelete: Bool = false
    @State var toDelete: (ElementID, String)?
    @Binding var tabSelection: Int
    
    let mapViewState: MapViewState
    
    init(_ mapViewState: MapViewState, _ tabSelection: Binding<Int>) {
        self.mapViewState = mapViewState
        self._tabSelection = tabSelection
    }
    
    var body: some View {
        VStack {
            HStack {
                Text("changeset.changeset")
                    .padding()
                    .bold()
                    .foregroundColor(.accentColor)
                    .dynamicTypeSize(.xxLarge)
                Spacer()
                Button(action: {
                    self.confirmReset = true
                }, label: {
                    Text("changeset.reset").bold().padding()
                })
                .disabled(self.confirmCommit || self.confirmReset || self.changeset.modify.isEmpty)
            }
            List(self.changeset.modify, id: \.id) { entry in
                Button(action: {
                    self.mapViewState.cameraPosition(
                        TGCameraPosition(
                            center: CLLocationCoordinate2D(latitude: entry.location.lat, longitude: entry.location.lon),
                            zoom: 16.0,
                            bearing: 0,
                            pitch: 0
                        )
                    )
                    DispatchQueue.main.async {
                        self.tabSelection = 0
                    }
                }, label: {
                    VStack {
                        HStack {
                            switch entry.element {
                            case .way(let way):
                                Text("changeset.way").bold().padding(.init(top: 18.0, leading: 18.0, bottom: 18.0, trailing: 0.0))
                                Text(verbatim: "\(String(way.id)) (\(entry.location.lat), \(entry.location.lon))").padding(.init(top: 18.0, leading: 0.0, bottom: 18.0, trailing: 18.0))
                                Spacer()
                            case .node(let node):
                                Text("changeset.node").bold().padding(.init(top: 18.0, leading: 18.0, bottom: 18.0, trailing: 0.0))
                                Text(verbatim: "\(String(node.id)) (\(entry.location.lat), \(entry.location.lon))").padding(.init(top: 18.0, leading: 0.0, bottom: 18.0, trailing: 18.0))
                                Spacer()
                            }
                        }
                        HStack {
                            Text("changeset.quest").bold().padding(.init(top: 18.0, leading: 18.0, bottom: 18.0, trailing: 0.0))
                            Text(Quest.quests[entry.questID!]?.name ?? "Unk").padding(.init(top: 18.0, leading: 0.0, bottom: 18.0, trailing: 18.0))
                            Spacer()
                        }
                    }.swipeActions {
                        Button(action: {
                            self.toDelete = (entry.element.elementID(), entry.questID!)
                            self.confirmDelete = true
                        }, label: {
                            Label("changeset.delete", systemImage: "minus")
                        })
                        .tint(Color.red)
                    }
                })
            }
            Spacer()
            Button(action: {
                self.confirmCommit = true
            }, label: {
                Text("changeset.commit").bold().padding()
            })
            .disabled(self.confirmCommit || self.confirmReset || self.changeset.modify.isEmpty)
        }.confirmationDialog("changeset.commit", isPresented: self.$confirmCommit, actions: {
            Button("changeset.cancel", role: .cancel) {
                self.confirmCommit = false
            }
            Button("changeset.confirm", role: .destructive) {
                DispatchQueue.main.async {
                    Task {
                        do {
                            try await self.changeset.submitChangeset()
                        } catch let error {
                            print("Error whilst committing changeset...", error)
                        }
                        self.confirmCommit = false
                    }
                }
            }
        }).confirmationDialog("changeset.reset", isPresented: self.$confirmReset, actions: {
            Button("changeset.cancel", role: .cancel) {
                self.confirmReset = false
            }
            Button("changeset.confirm", role: .destructive) {
                Task {
                    self.changeset.reset()
                    self.confirmReset = false
                }
            }
        }).confirmationDialog("changeset.delete", isPresented: self.$confirmDelete, actions: {
            Button("changeset.cancel", role: .cancel) {
                self.confirmDelete = false
            }
            Button("changeset.delete", role: .destructive) {
                let (elementID, questID) = self.toDelete!
                Task {
                    try await self.changeset.removeUpdate(elementID, questID)
                    self.confirmDelete = false
                }
            }
        })
    }
}
