//
//  MapUILayer.swift
//  OpenStreetQuests
//
//  Created by Elise on 03/01/2023.
//

import Foundation
import SwiftUI
import TangramMap

class MapViewState: NSObject, ObservableObject, TGMapViewDelegate, TGRecognizerDelegate {
    var mapView: TGMapView = TGMapView(frame: .zero)
    var selfMarkerLayer: TGMapData?
    
    func rerender() {
        self.mapView.cameraPosition = self.mapView.cameraPosition
        self.mapView.requestRender()
    }
    
    private static let selfMarkerLayerName: String = "osq_self"
    
    var questPointsMapLayer: QuestPointsMapLayer
    
    var checkIsSolved: ((_ elementID: ElementID, _ questID: String) -> Bool)?
    /// This will be called by the main thread by default
    var questSelectedCalback: ((ActiveQuest) -> Void)?
    
    var userInfo: UserInfo
    
    var previousCameraLocation: TGCameraPosition?
    
    init(_ userInfo: UserInfo) {
        self.userInfo = userInfo
        self.questPointsMapLayer = QuestPointsMapLayer(self.mapView)
        self.selfMarkerLayer = self.mapView.addDataLayer(Self.selfMarkerLayerName, generateCentroid: false)!
        super.init()
        self.mapView.mapViewDelegate = self
        self.mapView.gestureDelegate = self
    }
    
    func mapView(_ view: TGMapView!, recognizer: UIGestureRecognizer!, didRecognizeSingleTapGesture location: CGPoint) {
        self.mapView.pickLabel(at: location)
    }
    
//    func mapView(_ mapView: TGMapView, didSelectFeature feature: [String : String]?, atScreenPosition position: CGPoint) {
//        if let feature = feature {
//            print("Feature Selected:", feature)
//            print("Feature Position:", position)
//        }
//    }
    
    func mapView(_ mapView: TGMapView, didSelectLabel labelPickResult: TGLabelPickResult?, atScreenPosition position: CGPoint) {
        if let labelPickResult = labelPickResult {
            print("Label Selected:", labelPickResult.properties)
            let questID = labelPickResult.properties["questID"]!
            let qSID = labelPickResult.properties["QSID"]!
            let elementID: ElementID
            
            if let nodeID = labelPickResult.properties["nodeID"] {
                elementID = .node(Int(nodeID)!)
            } else {
                elementID = .way(Int(labelPickResult.properties["wayID"]!)!)
            }
            
            let elementName = labelPickResult.properties["elementName"]
            let quest = Quest.quests[questID]!;
            
            if self.previousCameraLocation == nil {
                self.previousCameraLocation = self.mapView.cameraPosition
            }
            
            self.mapView.setCameraPosition(TGCameraPosition(
                center: labelPickResult.coordinate,
                zoom: 17.0,
                bearing: 0.0,
                pitch: 0.0
            ), withDuration: 0.5, easeType: TGEaseType.linear)
            
            if let callback = self.questSelectedCalback {
                callback(ActiveQuest(id: questID, quest: quest, querySolution: quest.querySolution[qSID]!, elementID: elementID, questID: questID, elementName: elementName, location: labelPickResult.coordinate))
            }
            
        }
    }
    
//    func mapView(_ mapView: TGMapView, didSelectMarker markerPickResult: TGMarkerPickResult?, atScreenPosition position: CGPoint) {
//        if let markerPickResult = markerPickResult {
//            print("Marker Selected:", markerPickResult)
//            print("Marker Position:", position)
//        }
//    }
    
    func makeBoundingBox() -> (CLLocationCoordinate2D, CLLocationCoordinate2D) {
        let vBounds = self.mapView.coordinateSpace.bounds

        let vLowerLeft = CGPoint(x: vBounds.minX, y: vBounds.minY)
        let vUpperRight = CGPoint(x: vBounds.maxX, y: vBounds.maxY)

        let lowerLeft = self.mapView.coordinate(fromViewPosition: vLowerLeft)
        let upperRight = self.mapView.coordinate(fromViewPosition: vUpperRight)

        return (lowerLeft, upperRight)
    }
    
    func updateQuests(_ bbox: (CLLocationCoordinate2D, CLLocationCoordinate2D)) async {
        let zoom = await self.mapView.cameraPosition.zoom
        let latDiff = bbox.0.latitude - bbox.1.latitude;
        let lonDiff = bbox.0.longitude - bbox.1.longitude;
        if (zoom < 15.0 || zoom > 17.0) || (latDiff > 0.5 || latDiff < -0.5 || lonDiff > 0.5 || lonDiff < -0.5) {
            return
        }
        
        do {
            var questPoints: [QuestPoint] = []
            
            let (queryRes, bboxContents): ([String:[String:[OSMCodableElement]]], [OSMCodableElement]) = try await OSMQuery.sendQueries(self.userInfo, bbox)
            
            for (questID, qSElements) in queryRes {
                for (qSID, elements) in qSElements {
                    for element in elements {
                        if let isSolved = self.checkIsSolved {
                            if isSolved(element.elementID(), questID) {
                                continue
                            }
                        }
                        switch element {
                        case .node(let node):
                            var props = ["questID" : questID, "nodeID": String(node.id), "QSID": qSID]
                            
                            if let name = node.tags?["name"] {
                                props["elementName"] = name
                            }
                            
                            questPoints.append(QuestPoint(quest: Quest.quests[questID]!, location: CLLocationCoordinate2D(latitude: node.lat, longitude: node.lon), properties: props))
                        case .way(let way):
                            var props = ["questID" : questID, "wayID": String(way.id), "QSID": qSID]
                            
                            if let name = way.tags?["name"] {
                                props["elementName"] = name
                            } else if let street = way.tags?["addr:street"] {
                                if let number = way.tags?["addr:housenumber"] {
                                    props["elementName"] = "\(number) \(street)"
                                } else if let name = way.tags?["addr:housename"] {
                                    props["elementName"] = "\(name), \(street)"
                                }
                            }
                            
                            if way.nodes.isEmpty {
                                continue
                            }
                            
                            guard let nodes = way.nodes(bboxContents: bboxContents) else { continue }
                            let centerLocation = way.centerLocation(nodes)
                            
                            questPoints.append(QuestPoint(quest: Quest.quests[questID]!, location: centerLocation, properties: props))
                        }
                    }
                }
            }
            
            self.questPointsMapLayer.set(questPoints)
        } catch let qError {
            print("Error whilst updating quests:", qError)
        }
    }

    func updateSelfCoordinates(_ coords: CLLocationCoordinate2D) {
        self.selfMarkerLayer!.setFeatures([TGMapFeature(point: coords, properties: ["colour":"#ffffff", "size": "28px", "priority": "1"]), TGMapFeature(point: coords, properties: ["colour":"#55cdfc", "size": "22px", "priority": "2"])])
    }
    
    func cameraPosition(_ cameraPosition: TGCameraPosition) {
        self.mapView.cameraPosition = cameraPosition
    }
}

struct BaseMapView: UIViewRepresentable {
    @EnvironmentObject var state: MapViewState
    @State var loaded: Bool = false
 
    func makeUIView(context: Context) -> TGMapView {
        if !self.loaded {
            let apiKey = Bundle.main.infoDictionary?["NextzenApiKey"] as! String
            let sceneUpdates = [TGSceneUpdate(path: "global.sdk_api_key", value: apiKey)]
            let sceneURL = Bundle.main.url(forResource: "bubble-wrap/bubble-wrap-style", withExtension: "yaml")!//URL(string: "https://www.nextzen.org/carto/bubble-wrap-style/9/bubble-wrap-style.zip")!
            self.state.mapView.loadScene(from: sceneURL, with: sceneUpdates)
            DispatchQueue.main.async {
                self.loaded = true
            }
        }
        return self.state.mapView
    }
 
    func updateUIView(_ uiView: TGMapView, context: Context) {
        
    }
}
