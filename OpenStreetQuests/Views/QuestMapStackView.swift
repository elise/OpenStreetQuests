//
//  QuestMapStackView.swift
//  OpenStreetQuests
//
//  Created by Elise on 07/01/2023.
//

import SwiftUI
import TangramMap

struct ActiveQuest {
    let id: String
    let quest: Quest
    let querySolution: QuerySolution
    let elementID: ElementID
    let questID: String
    let elementName: String?
    let location: CLLocationCoordinate2D
}

struct QuestMapStackView: View {
    @EnvironmentObject var changeset: CurrentChangeset
    @EnvironmentObject var locationHelper: LocationHelper
    @State var activeQuest: ActiveQuest?
    @State var questSelected: Bool = false
    @State var mapViewState: MapViewState
    
    init(_ mapViewState: MapViewState) {
        self.mapViewState = mapViewState
    }
    
    func exitQuest() {
        DispatchQueue.main.async {
            self.questSelected = false
            self.activeQuest = nil
        }
        if let location = self.mapViewState.previousCameraLocation {
            self.mapViewState.mapView.setCameraPosition(location, withDuration: 0.5, easeType: TGEaseType.linear)
            self.mapViewState.previousCameraLocation = nil
        }
    }
    
    var body: some View {
        ZStack {
            MapView(
                mapViewState: mapViewState
            ).environmentObject(self.locationHelper)
            VStack {
                if let activeQuest = self.activeQuest {
                    Spacer()
                    switch activeQuest.querySolution.solution {
                    case .yesNo(let yesNo):
                        YesNoQuestView(elementID: activeQuest.elementID, key: yesNo.destinationKey, yesValue: yesNo.yesValue, noValue: yesNo.noValue, questID: activeQuest.questID, questName: activeQuest.quest.name, question: yesNo.question, elementName: activeQuest.elementName, location: activeQuest.location, exit: self.exitQuest).environmentObject(self.changeset)
                            .environmentObject(self.mapViewState)
                            .background(.thinMaterial)
                    case .count(let count):
                        CountQuestView(elementID: activeQuest.elementID, key: count.destinationKey, questID: activeQuest.questID, questName: activeQuest.quest.name, question: count.question, elementName: activeQuest.elementName, location: activeQuest.location, exit: self.exitQuest).environmentObject(self.changeset)
                            .environmentObject(self.mapViewState)
                            .background(.thinMaterial)
                    case .selection(let selection):
                        SelectionQuestView(elementID: activeQuest.elementID, destinationKey: selection.destinationKey, questID: activeQuest.questID, questName: activeQuest.quest.name, question: selection.question, entries: selection.entries, elementName: activeQuest.elementName, location: activeQuest.location, exit: self.exitQuest)
                            .environmentObject(self.mapViewState)
                            .background(.thinMaterial)
                    }
                    
                }
            }
            .onAppear {
                self.mapViewState.checkIsSolved = { (elementID, questID) in
                    return self.changeset.isSolved(elementID, questID)
                }
                
                self.mapViewState.questSelectedCalback = { (activeQuest) in
                    self.activeQuest = activeQuest
                    self.questSelected = true
                }
            }
            .animation(Animation.easeInOut(duration: 0.3), value: self.questSelected)
        }
    }
}
