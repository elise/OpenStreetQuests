//
//  MapView.swift
//  OpenStreetQuests
//
//  Created by Elise on 05/01/2023.
//

import SwiftUI
import TangramMap

struct MapView: View {
    @EnvironmentObject var locationHelper: LocationHelper
    @EnvironmentObject var currentChangeset: CurrentChangeset
    @State var mapViewState: MapViewState
    @State var locationSet: Bool = false
    
    var body: some View {
        VStack {
            if self.locationHelper.location != nil {
                ZStack {
                    BaseMapView()
                        .edgesIgnoringSafeArea(.all)
                        .environmentObject(self.mapViewState)
                        .overlay(
                            HStack {
                                Text("Map Data ©").foregroundColor(.primary)
                                Link("OpenStreetMap Contributors", destination: URL(string: "https://www.openstreetmap.org/copyright")!).foregroundColor(.primary)
                            }.font(.system(size: 12, weight: .light, design: .default))
                                .background(Color.init(Color.RGBColorSpace.sRGB, white: 0.5, opacity: 0.8))
                                .cornerRadius(1),
                            alignment: .bottomTrailing)
                    VStack {
                        Spacer()
                        HStack {
                            Spacer()
                            VStack {
                                Button(action: {
                                    Task {
                                        let bbox = self.mapViewState.makeBoundingBox()
                                        await self.mapViewState.updateQuests(bbox)
                                        self.mapViewState.cameraPosition(self.mapViewState.mapView.cameraPosition)
                                    }
                                }, label: {
                                    Image(systemName: "magnifyingglass.circle.fill")
                                        .resizable()
                                        .frame(width: 44, height: 44, alignment: .trailing)
                                        .padding(EdgeInsets(top: 0.0, leading: 18.0, bottom: 0.0, trailing: 18.0))
                                }).disabled(self.mapViewState.mapView.cameraPosition.zoom < 15.0 || self.mapViewState.mapView.cameraPosition.zoom > 17.0)
                                Button(action: {
                                    if let location = self.locationHelper.location {
                                        self.mapViewState.cameraPosition(TGCameraPosition(
                                            center: location.coordinate,
                                            zoom: 16.0,
                                            bearing: 0,
                                            pitch: 0))
                                    }
                                }, label: {
                                    Image(systemName: "location.circle.fill")
                                        .resizable()
                                        .frame(width: 44, height: 44, alignment: .trailing)
                                        .padding(EdgeInsets(top: 0.0, leading: 18.0, bottom: 0.0, trailing: 18.0))
                                })
                                Button(action: {
                                    let pos = self.mapViewState.mapView.cameraPosition
                                    pos.zoom += 0.1
                                    self.mapViewState.mapView.cameraPosition = pos
                                }, label: {
                                    Image(systemName: "plus.circle.fill")
                                        .resizable()
                                        .frame(width: 44, height: 44, alignment: .trailing)
                                        .padding(EdgeInsets(top: 0.0, leading: 18.0, bottom: 0.0, trailing: 18.0))
                                })
                                Button(action: {
                                    let pos = self.mapViewState.mapView.cameraPosition
                                    pos.zoom -= 0.1
                                    self.mapViewState.mapView.cameraPosition = pos
                                }, label: {
                                    Image(systemName: "minus.circle.fill")
                                        .resizable()
                                        .frame(width: 44, height: 44, alignment: .trailing)
                                        .padding(EdgeInsets(top: 0.0, leading: 18.0, bottom: 18.0, trailing: 18.0))
                                })

                            }
                        }
                    }
                }
            } else {
                Text("Location loading...")
                    .foregroundColor(.accentColor)
            }
        }.onAppear {
            self.locationHelper.locationUpdateCallback = { (location) in
                if !self.locationSet {
                    self.mapViewState.cameraPosition(TGCameraPosition(
                    center: location.coordinate,
                    zoom: 16.0,
                    bearing: 0,
                    pitch: 0))
                    self.locationSet = true
                    let bbox = self.mapViewState.makeBoundingBox()
                    Task {
                        await self.mapViewState.updateQuests(bbox)
                    }
                }
                mapViewState.updateSelfCoordinates(location.coordinate)
            }
        }.onDisappear {
            self.locationHelper.locationUpdateCallback = nil
        }
    }
}
