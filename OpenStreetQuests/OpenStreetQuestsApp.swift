//
//  OpenStreetQuestsApp.swift
//  OpenStreetQuests
//
//  Created by Elise on 03/01/2023.
//

import SwiftUI
import SafariServices

struct OpenStreetQuestsMainView: View {
    @State var user: UserInfo? = UserInfo.read()
    @State var locationHelper: LocationHelper = LocationHelper()
    @State var error: String?
    @State var verifiedUsername: Bool = false
    @State var safariViewPresented: Bool = false
    
    init() {
        let _ = Quest.quests.count
    }
    
    var body: some View {
        Group {
            if let user = self.user, self.verifiedUsername {
                TabbedView(userInfo: user, {
                    DispatchQueue.main.async {
                        self.error = nil
                        self.user = nil
                        self.verifiedUsername = false
                    }
                }).environmentObject(self.locationHelper)
            } else if let user = self.user, !self.verifiedUsername {
                VStack {
                    HStack {
                        Text("OpenStreetQuests")
                            .dynamicTypeSize(.xLarge)
                            .bold()
                            .padding()
                            .foregroundColor(.accentColor)
                        Spacer()
                    }
                    Spacer()
                    Text("login.loading")
                        .bold()
                        .padding()
                        .foregroundColor(.accentColor)
                    Spacer()
                }.task {
                    do {
                        let details = try await OSMUtils.userDetails(user.token)
                        
                        let _ = details.write()
                        
                        DispatchQueue.main.async {
                            self.user = details
                            self.verifiedUsername = true
                        }
                    } catch let error {
                        print("Error whilst fetching user info with cached token, logging out... (error: \(error))")
                        UserInfo.clear()
                        DispatchQueue.main.async {
                            self.user = nil
                            self.verifiedUsername = false
                        }
                    }
                }
            } else {
                LoginView(error: self.$error, safariViewPresented: self.$safariViewPresented)
            }
        }.onOpenURL(perform: { url in
            self.safariViewPresented = false
            
            let host = url.host() ?? "";
            
            if host == "oauth2" {
                let queryParameters = (url.query() ?? "").split(separator: "&").map { substr in
                    return substr.split(separator: "=")
                }.filter { array in
                    return array.count == 2
                }.map { array in
                    return (array[0], array[1])
                }
                
                guard let state = queryParameters.first(where: { (key, _) in
                    return key == "state"
                })?.1 else { return }
                
                guard let expectedState = OAuth2.state else { return }
                
                if state != expectedState {
                    DispatchQueue.main.async {
                        self.error = "errors.login.invalidState"
                        OAuth2.state = nil
                    }
                    return
                }
                
                if let error = queryParameters.first(where: { (key, _) in
                    return key == "error"
                })?.1 {
                    var errorMessage = error;
                    
                    if let errorDescription = queryParameters.first(where: { (key, _) in
                        return key == "error_description"
                    })?.1 {
                        errorMessage += ": \(errorDescription)"
                    }
                    
                    DispatchQueue.main.async {
                        self.error = String(errorMessage)
                    }
                } else if let code = queryParameters.first(where: { (key, _) in
                    return key == "code"
                })?.1 {
                    Task {
                        do {
                            let userInfo = try await OAuth2.tradeCode(String(code))
                            
                            if userInfo.write() {
                                print("Failed to write userinfo")
                            }
                            
                            DispatchQueue.main.async {
                                self.verifiedUsername = true
                                self.user = userInfo
                            }
                        } catch let error {
                            DispatchQueue.main.async {
                                self.error = error.localizedDescription
                            }
                        }
                    }
                }
            }
        })
    }
}

@main
struct OpenStreetQuestsApp: App {
    let appearance: UITabBarAppearance = UITabBarAppearance()
    
    init() {
        UITabBar.appearance().scrollEdgeAppearance = appearance
    }
    
    var body: some Scene {
        WindowGroup {
            OpenStreetQuestsMainView()
        }
    }
}
