#  Privacy Policy

In order to use the app you need to sign-in to OpenStreetMap. Your token and username are stored locally after a sucessful login until you sign-out.

Your GPS location is used to fetch map tiles from the Nextzen vector tile server, and to fetch information about your surroundings from the OpenStreetMap server to display quests around you. 

I do not run any servers that collect any data from you.
