#  OpenStreetQuests

## Description

OpenStreetQuests is a quest based OpenStreetMap editor that was inspired by the Android app StreetComplete.

## Features

* Focus on quests that improve accessibility
* Query engine for quests, where queries are build in JSON
* Changeset building

## Screenshots

<img src="./Screenshots/iPhone 13 Pro/final/MapView.png" width="223">
<img src="./Screenshots/iPhone 13 Pro/final/AccessibilityQuest.png" width="223">
<img src="./Screenshots/iPhone 13 Pro/final/Changeset.png" width="223">
<img src="./Screenshots/iPhone 13 Pro/final/RegularQuest.png" width="223">

## Privacy

The (needed for publishing) privacy policy can be found in [PRIVACY.md](./PRIVACY.md)

## Credits

* [StreetComplete](https://github.com/streetcomplete/StreetComplete) for inspiration
* [OpenStreetMap](https://openstreetmap.org//) for map data (API used)
* [Nextzen](https://www.nextzen.org/) for map tiles (API used)
* [tangram-es](https://github.com/tangrams/tangram-es) for map rendering (used as a pod)
* [bubble-wrap](https://github.com/tangrams/bubble-wrap) theme for tangram-es (embedded in repo at `OpenStreetQuests/bubble-wrap`, with a file added)
* [SwiftyXML](https://cocoapods.org/pods/SwiftyXML) for XML parsing (used as a pod)
